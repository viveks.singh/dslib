#ifndef __SLIST_H__
#define __SLIST_H__

typedef int (*walk_cb) (void *data, void *user_data);
typedef int (ds_slist_compre_cb) (void *data1, void *data2, void *user_data);


void* ds_slist_create();
void* ds_slist_destroy(void *handler);


int ds_slist_append (void *handler, void *data);
int ds_slist_remove (void *handler, void *data);
void* ds_slist_pop (void *handler);
int ds_slist_length (void *handler);
int ds_slist_reverse(void *handler);
void ds_slist_walk (void *handler, walk_cb walk, void *user_data);
void ds_slist_sort (void *handler);
int ds_slist_append_list(void *to_handler, void *from_handler);

int ds_slist_sublist(void *handler, void *sublist_handler, ds_slist_compre_cb compare, void *user_data);




#endif /*  __SLIST_H__ */
