#include <malloc.h>
#include "slist.h"

typedef struct SListNode {
    void *data;
    struct SListNode *next;
}SListNode;

typedef struct DSSList {

    /* Data */
    SListNode *head;
    int count;

}DSSList;

static SListNode* slist_node_create (void *data) {
    SListNode *ptr = (SListNode*)malloc(sizeof(SListNode));
    if (ptr) {
        ptr->data = data;
        ptr->next = NULL;
    }
    return ptr;
}

void*
ds_slist_create() {
    DSSList *handler = (DSSList*)malloc(sizeof(DSSList));
    if (handler) {

        handler->head = 0;
    }
    return (void*)handler;
}

void*
ds_slist_destroy(void *handler) {
    DSSList *hdlr = (DSSList *)handler;
    SListNode *p = hdlr->head;
    SListNode *q = hdlr->head;

    while (p) {
        q = p->next;
        free(p);
        p = q;
    }
    hdlr->head = 0;
    free(hdlr);
    hdlr = 0;
    return hdlr;
}

/* 0 For success else Failure */
int
ds_slist_append (void *handler, void *data) {

    SListNode *ptr = NULL;
    SListNode *p = NULL;
    DSSList *hdlr = (DSSList *)handler;

    if (!hdlr)
        return -1;


    ptr = slist_node_create (data);
    if (!ptr)
        return -1;

    if (!hdlr->head) {
        hdlr->head = ptr;
        hdlr->count++;
        return 0;
    }

    p = hdlr->head;
    while (p->next) {
        p = p->next;
    }

    p->next = ptr;
    hdlr->count++;
    return 0;
}

/* 0 For success else Failure */
int
ds_slist_remove (void *handler, void *data) {

    SListNode *p = NULL;
    SListNode *q = NULL;
    DSSList *hdlr = (DSSList *)handler;

    if (!hdlr)
        return -1;

    if (!hdlr->head) {
        return -1;
    }

    if (hdlr->head->data == data) { // Remove head 
        p = hdlr->head;
        hdlr->head = p->next;
        free(p);
        hdlr->count--;
        return 0;
    }

    p = hdlr->head;
    q = p->next;
    while (q) {
        if (q->data == data)
            break;
        p = q;
        q = q->next;
    }

    if (!q) { // Not found
        return -1;
    }

    p->next = q->next;
    free(q);
    hdlr->count--;
    return 0;
}

void*
ds_slist_pop (void *handler) {
    SListNode *p = NULL;
    SListNode *q = NULL;
    SListNode *r = NULL;
    void *data = 0;
    DSSList *hdlr = (DSSList *)handler;

    if (!hdlr)
        return 0;

    if (!hdlr->head) {
        return 0;
    }


    p = hdlr->head;
    q = p->next;

    if (q == 0) {
        hdlr->head = 0;
        data = p->data;
        free(p);
        hdlr->count--;
        return data;
    }

    r = q->next;
    while (r) {
        p = q;
        q = r;
        r = r->next;
    }

    data = q->data;
    free(q);
    p->next = 0;
    hdlr->count--;
    return data;
}

int
ds_slist_length (void *handler) {
    DSSList *hdlr = (DSSList *)handler;
    if (!hdlr)
        return -1;
    return hdlr->count;
}

int
ds_slist_reverse(void *handler) {
    SListNode *p = 0;
    SListNode *q = 0;
    SListNode *r = 0;
    DSSList *hdlr = (DSSList *)handler;

    if (!hdlr)
        return -1;

    if (!hdlr->head) {
        return 0;
    }

    q = hdlr->head;
    r = q;

    while (r) {
        r = q->next;
        q->next = p;
        p = q;
        q = r;
    }
    hdlr->head = p;
    return 0;
}

void
ds_slist_walk (void *handler, walk_cb walk, void *user_data) {
    DSSList *hdlr = (DSSList *)handler;
    SListNode *p = 0;

    if (!hdlr)
        return;

    p = hdlr->head;
    while (p) {
        if (!walk(p->data, user_data)) /* Break if user returs false */
            break;
        p = p->next;
    }
}

void
ds_slist_sort (void *handler) {
    DSSList *hdlr = (DSSList *)handler;
    SListNode *p = 0;
}

int
ds_slist_append_list (void *to_handler, void *from_handler) {
    DSSList *to_hdlr = (DSSList *)to_handler;
    DSSList *from_hdlr = (DSSList *)from_handler;
    SListNode *p = 0;

    if (!from_hdlr || !to_hdlr)
        return -1;

    if (!from_hdlr->head) {
        return 0;
    }

    p = from_hdlr->head;
    while (p) {
        ds_slist_append(to_handler, p->data);
        p = p->next;
    }
    return 0;
}

int
ds_slist_sublist (void *handler, void *sublist_handler, ds_slist_compre_cb compare, void *user_data) {
    DSSList *hdlr = (DSSList *)handler;
    DSSList *sublist_hdlr = (DSSList *)sublist_handler;
    SListNode *p = 0;
    SListNode *q = 0;
    SListNode *r = 0;

    if (!compare)
        return 0;

    if (!hdlr || !sublist_hdlr)
        return 0;

    if (!hdlr->head || !sublist_hdlr->head)
        return 0;

    p = hdlr->head;
    q = hdlr->head;
    r = sublist_hdlr->head;
    while (p) {
        q = p;
        r = sublist_hdlr->head;

        while (r && q && compare(q->data, r->data, user_data)) {
            r = r->next;
            q = q->next;
        }
        if (!r) {
            return 1;
        }
        p = p->next;
    }
    return 0;
}
