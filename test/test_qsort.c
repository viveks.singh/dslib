#include <stdio.h>
#include <malloc.h>

#include "algo/qsort.h"


void
arr_dump(int *arr, int low, int high) {

    for (; low<= high; low++) {
        printf("%d ", arr[low]);
    }
    printf("\n");
}

int
main() {

    int len = 1024;
    int i = 0;
    int *arr = (int*)malloc(sizeof(int)*len);
    for (i=0; i<len; i++) {
        arr[i] = len-i;
    }

    //int arr[] = { 6, 8, 4, 3, 12, 1 };
    //int len = sizeof(arr)/sizeof(int);


    //arr_dump(arr, 0, len-1);
    ds_qsort(arr, 0, len-1);
    arr_dump(arr, 0, len-1);

    return 0;
}
