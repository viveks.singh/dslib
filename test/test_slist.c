#include <stdio.h>
#include "slist/slist.h"

int walk (void *data, void *user_data) {
    user_data = 0;
    printf ("%d\t", data);
    return 1;
}

int
compare (void *data1, void *data2, void *user_data) {
    user_data = 0;
    if ((long)data1 == (long)data2)
        return 1;
    return 0;
}


int
main() {

    void *ds1 = ds_slist_create();
    void *ds2 = ds_slist_create();
    int i = 0;

    for (i=1; i<=10; i++) {
        ds_slist_append(ds1, (void*)i);
    }

    for (i=2; i<=7; i++) {
        if (i == 5)
            ds_slist_append(ds2, (void*)(i*10));
        else
            ds_slist_append(ds2, (void*)i);
    }

    
    printf ("Length: %d\n", ds_slist_length(ds1));

    ds_slist_walk(ds1, walk, NULL);
    printf ("\n");

    ds_slist_walk(ds2, walk, NULL);
    printf ("\n");

    // Reverse list
    // ds_slist_reverse(ds1);

    // Append ds2 to ds1
    // ds_slist_append_list(ds1, ds2);

    ds_slist_walk(ds1, walk, NULL);
    printf ("\n");

    ds_slist_walk(ds2, walk, NULL);
    printf ("\n");

    printf ("Sublist: %s\n", (ds_slist_sublist (ds1, ds2, compare, 0)?"true":"false"));

    ds1 = ds_slist_destroy(ds1);
    ds2 = ds_slist_destroy(ds2);
    return 0;
}
