#include <stdio.h>

#include "stack/dsstack.h"


int
main() {

    void *ds = ds_stack_create();

    printf("%p\n", ds_stack_pop(ds));
    ds_stack_push(ds, (void*)1);
    ds_stack_push(ds, (void*)2);
    ds_stack_push(ds, (void*)3);

    printf("%p\n", ds_stack_pop(ds));
    printf("%p\n", ds_stack_pop(ds));
    printf("%p\n", ds_stack_pop(ds));
    printf("%p\n", ds_stack_pop(ds));

    ds = ds_stack_destroy(ds);
    return 0;
}
