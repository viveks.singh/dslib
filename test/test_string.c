#include <stdio.h>
#include <malloc.h>

#include "slist/slist.h"
#include "string/dsstring.h"

int walk (void *data, void *user_data) {
    if (user_data)
        free(user_data);

    printf ("%s\n", (char*)data);
    free(data);
    return 1;
}


int
main() {

    void *ds = ds_slist_create();
    char str[124] = {0};

    snprintf(str, 124, "Vivek;Singh;From;Varanasi");
    printf ("%s\n", str);

    ds_string_split(str, ';', ds);
    ds_slist_walk(ds, walk, NULL);
    ds = ds_slist_destroy(ds);
    printf ("%s\n", str);

    return 0;
}
