#include "dsstack.h"
#include "malloc.h"
#include "slist/slist.h"

typedef struct DSStack {

    void *lst;

}DSStack;

void*
ds_stack_create() {
    DSStack *handler = (DSStack*)malloc(sizeof(DSStack));
    if (handler) {
        handler->lst = ds_slist_create();
    }

    return (void*)handler;
}

void*
ds_stack_destroy(void* handler) {
    DSStack *hdlr = (DSStack *)handler;
    if (!hdlr)
        return 0;

    hdlr->lst = ds_slist_destroy(hdlr->lst);
    free(hdlr);
    return 0;
}


int ds_stack_push (void* handler, void *data) {
    DSStack *hdlr = (DSStack *)handler;
    if (!hdlr)
        return 0;

    // printf ("%s %p\n", __func__, data);
    return ds_slist_append(hdlr->lst, data);
}

void*
ds_stack_pop (void *handler) {
    DSStack *hdlr = (DSStack *)handler;
    if (!hdlr)
        return 0;

    // printf ("%s %p\n", __func__, handler);
    return ds_slist_pop(hdlr->lst);
}


int ds_stack_empty (void *handler) {
    DSStack *hdlr = (DSStack *)handler;
    if (!hdlr)
        return 0;

    // printf ("%s %p\n", __func__, handler);
    return (ds_slist_length(hdlr->lst) == 0);
}
