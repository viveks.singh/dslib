#ifndef __DS_STACK__
#define __DS_STACK__



void* ds_stack_create();
void* ds_stack_destroy(void* handler);


int ds_stack_push (void *handler, void *data);
void* ds_stack_pop (void *handler);
int ds_stack_empty (void *handler);


#endif /*  __DS_STACK__ */

