#include <stdio.h>
#include <malloc.h>

#include "stack/dsstack.h"


int
get_area (int *histogram, unsigned int len) {

    typedef struct Node {
        int val;
        int index;
    }Node;

    void *ds = ds_stack_create();
    unsigned int i = 0;
    unsigned int prev_index = 0;
    Node *m = 0;
    int max_area = 0;
    int this_area = 0;


    if (len<=0)
        return max_area;

    /*
        A. For each element in the array:
            while stack not empty:
                pop element from stack
                2.1 if current element is smaller than stack element
                    Calculate area of stack element.
                2.2 else
                    push popped element
                    break

            if current element is greater than zero then push current element into stack
            
        B. For each element in stack Calculate area by using (len-index) 
    */


    for (i=0; i<len; i++) { 
        m = 0;
        prev_index = i;
        while (!ds_stack_empty(ds)) {
            m = (Node*)ds_stack_pop(ds);

            if (histogram[i] < m->val) {
                this_area = m->val*(i-m->index);
                if (max_area < this_area)
                    max_area = this_area;
                // printf("%d area: %d\n", m->val, (m->val*(i-m->index)));
                prev_index = m->index;
                free(m);
            } else {
                ds_stack_push(ds, (void*)m);
                break;
            }
        }

        if (histogram[i] == 0) 
            continue;

        if (m && histogram[i] == m->val) 
            continue;

        Node *n = (Node*)malloc(sizeof(Node));
        n->val = histogram[i];
        n->index = prev_index;
        ds_stack_push(ds, (void*)n);
    }

    while (!ds_stack_empty(ds)) {
        Node *m = (Node*)ds_stack_pop(ds);
        // printf("%d Area: %d\n", m->val, (m->val*(len-m->index)));
        this_area = m->val*(len-m->index);
        if (max_area < this_area)
            max_area = this_area;
        free(m);
    }

    ds = ds_stack_destroy(ds);
    return max_area;
}

int
main() {

    int *histogram = 0;
    int T = 0;
    int N = 0;
    int i = 0;
    int j = 0;

    scanf("%d", &T);
    printf ("%d\n", T);

    for (i=0; i<T; i++) {
        scanf("%d", &N);
        histogram = (int*)malloc(sizeof(int)*N);

        for (j=0; j<N; j++) {
            scanf("%d", histogram+j);
        }

        printf ("%d\n", get_area(histogram, N));
    }

    return 0;
}
