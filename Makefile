# Makefile for shared library

CC = gcc # C compiler
LDFLAGS = -shared  # linking flags
RM = rm -f  # rm command
OBJ_DIR = obj/* # target lib
TARGET_LIB = libds.so # target lib


.PHONY: all
all:
	mkdir -p obj
	+$(MAKE) -C stack
	+$(MAKE) -C slist
	+$(MAKE) -C string 
	+$(MAKE) -C algo
	$(CC) ${LDFLAGS} -o libds.so ${OBJ_DIR}

.PHONY: test
test: all
	+$(MAKE) -C test


.PHONY: clean
clean:
	+$(MAKE) -C slist clean
	+$(MAKE) -C stack clean
	+$(MAKE) -C string clean
	+$(MAKE) -C algo clean
	+$(MAKE) -C test clean
	${RM} ${TARGET_LIB} ${OBJ_DIR}
	
