#include <stdio.h>
#include <stdlib.h>

#include "qsort.h"


void
ds_arr_dump(int *arr, int low, int high) {

    for (; low<= high; low++) {
        printf("%d ", arr[low]);
    }
    printf("\n");
}

#define DS_SWAP(x,y) \
do { \
    int tmp = x; \
    x = y; \
    y = tmp; \
}while(0); 


int
ds_partition(int *arr, int low, int high) {

    int pivot = (rand() % (high-low)) + low;
    int i = low-1;
    int j = low;

    DS_SWAP(arr[pivot] , arr[high]);
    pivot = high;

    for (; j<high; j++) {

        if (arr[j] < arr[pivot]) {
            i++;
            DS_SWAP(arr[i], arr[j]);
        }
    }

    i++;
    DS_SWAP(arr[i], arr[pivot]);

    return i;
}


void
ds_qsort(int *arr, int low, int high) {

    int mid = 0;

    if (low < high) {
        mid = ds_partition(arr, low, high);
        ds_qsort(arr, low, mid-1);
        ds_qsort(arr, mid+1, high);
    }
}

