#include <string.h>
#include <malloc.h>

#include "slist/slist.h"
#include "dsstring.h"

int
ds_string_split(char *src, char delimiter, void *list) {

    char *token = 0;
    char *ptr = 0;
    int len = 0;

    if (!src)
        return -1;

    if (!list)
        return -1;

    len = strlen(src);
    token = (char*)malloc((len*sizeof(char)) + 1);
    ptr = token;

    while (*src) {

        if (*src == delimiter) {
            *ptr = 0;
            if (ptr != token) {
                ds_slist_append(list, (void*)strdup(token));
            }

            /* reuse token*/
            ptr = token;
            src++;
        } else {
            *ptr = *src;
            ptr++;
            src++;
        }
    }

    if (ptr != token) {
        ds_slist_append(list, (void*)strdup(token));
    }

    free(token);
    return 0;
}
